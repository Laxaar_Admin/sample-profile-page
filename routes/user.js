var express = require('express');
var router = express.Router();
var userService = require('../services/user.service');
/* GET users listing. */
router.get('/', function(req, res, next) {
  return res.json(userService.getUser());
});
router.post('/',function(req,res,next){
  var body = req.body;
  if(body && body.name && body.city){
    return res.json(userService.saveUser(body.name,body.city));
  } else {
    return res.sendStatus(500,"Name or city is missing");
  }
})
module.exports = router;
