'use strict';
/**
 * Profile Service
 */
app.factory('profileService', ['$http', function($http) {
    var getUser = function() {
        return $http.get('/user').then(function(response) {
            return response.data
        });
    }
    var saveUser = function(user) {
        return $http.post('/user', user).then(function(response) {
            return response.data
        });
    }
    return {
        getUser: getUser,
        saveUser: saveUser
    }
}]);
