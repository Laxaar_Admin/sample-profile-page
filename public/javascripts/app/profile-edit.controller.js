'use strict';
/**
 * Profile Controller for the  page
 */
app.controller('ProfileEditCtrl', ["$scope", "$timeout", "$state", "user", "profileService", function($scope, $timeout, $state, user, profileService) {
    var ctrl = this;
    var profileModalSelector = '#profileModal';
    var initializeModal = function() {
        angular.element(profileModalSelector).openModal({
            complete: function() {
                $state.go('profile.view');
            }
        });
    };
    var closeModal = function(){
      $state.go('profile.view');
      angular.element(profileModalSelector).closeModal();
    }
    var saveUser = function() {
        ctrl.waiting = true;
        profileService.saveUser(ctrl.user).then(function(data) {
            ctrl.message = "Data Saved Successfully";
            $timeout(closeModal,2000);
        }, function(error) {
            console.log(error);
            ctrl.message = "There was an error in saving data";
            ctrl.waiting = false;
        });
    };
    var validateForm = function(form) {
      var result = form ? form.$valid : false;
      return result;
    };
    ctrl.saveUser = saveUser;
    ctrl.user = user;
    ctrl.focus={};
    ctrl.validateForm = validateForm
    $timeout(initializeModal, 0);
}]);
