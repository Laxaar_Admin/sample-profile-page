'use strict';
/**
  * Profile Controller for the  page
*/
app.controller('ProfileCtrl', ["$scope","$timeout","user",function ($scope,$timeout,user) {
  var ctrl = this;
  var createParallax = function(){
    angular.element('.parallax').parallax();
  }
  $timeout(createParallax);
  ctrl.user = user;
}]);
