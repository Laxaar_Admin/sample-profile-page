'use strict';

/**
 * Config for the router
 */
app.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
        // APPLICATION ROUTES
        // -----------------------------------
        // For any unmatched url, redirect to /
        $urlRouterProvider.otherwise("/profile/view");
        //
        // Set up the states
        $stateProvider
            .state('profile', {
                url: '/profile',
                templateUrl: "views/profile.html",
                abstract : true,
            }).state('profile.view', {
                url: '/view',
                templateUrl: "views/profileInfo.html",
                controller: 'ProfileCtrl',
                controllerAs : 'ProfileCtrl',
                resolve : {
                  user : ['profileService',function(profileService){
                      return profileService.getUser();
                  }]
                }
            }).state('profile.edit', {
                url: '/edit',
                templateUrl: "views/profileEdit.html",
                controller: 'ProfileEditCtrl',
                controllerAs : 'ProfileEditCtrl',
                resolve : {
                  user : ['profileService',function(profileService){
                      return profileService.getUser();
                  }]
                }
            });
    }
]);
