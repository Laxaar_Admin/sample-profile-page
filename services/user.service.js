var userService = (function(){
  var user = {
    name : 'Inderdeep Singh',
    city : 'Jalandhar'
  }
  var getUser = function(){
    return user;
  };
  var saveUser = function(name,city){
    user.name = name;
    user.city = city;
    return user;
  }
  return {
    getUser : getUser,
    saveUser : saveUser,
  }
})();
module.exports = userService;
